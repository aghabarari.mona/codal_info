﻿using DomainModel.Entities.PartyAgreegate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DomainModel.Entities
{
    public class InformationModel
    {
        protected InformationModel()
        {

        }
        public InformationModel(string endTime, string symbol, string startDate, string day,
                                string date, string address, string order)
        {
            EndTime = endTime;
            Symbol = symbol;
            StartDate = startDate;
            Day = day;
            Date = date;
            Address = address;
            Order = order;
        }
        public int Id { get;private set; }
        public string EndTime { get; private set; }
        public CompanyModel Company { get; private set; }
        public string Symbol { get; private set; }
        public string StartDate { get; private set; }
        public string StartTime { get; private set; }
        public string Day { get; private set; }
        public string Date { get; private set; }
        public string Address { get; private set; }
        public string Order { get; private set; }
        public DecisionsModel Decisions { get; private set; }

        public void SetCompany(CompanyModel company)
        {
            this.Company = company;
        }

        public void SetDecisionsModel(DecisionsModel decision)
        {
            this.Decisions = decision;
        }

        private readonly List<AudienceModel> _audiences;

        public IReadOnlyCollection<AudienceModel> Audience => _audiences;
        public void AddAudience(AudienceModel audienceModel)
        {
            _audiences.Add(audienceModel);
        }

    }
}