﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DomainModel.Entities
{
    public class AudienceModel
    {
        private AudienceModel()
        {

        }
        public AudienceModel(string audienceName, string audienceCount, string audiencePerCent)
        {
            this.AudienceName = audienceName;
            this.AudienceCount = audienceCount;
            this.AudiencePerCent = audiencePerCent;
        }
        public string AudienceName { get;private set; }
        public string AudienceCount { get; private set; }
        public string AudiencePerCent { get; private set; }
    }
}