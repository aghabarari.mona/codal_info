﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DomainModel.Entities
{
    public class DecisionsModel
    {
        private DecisionsModel()
        {

        }

        public DecisionsModel(int plpure, int plbeginPeriod, int disCount, int plbeginPeriodDisCount,
                              int sumLastYear, int changeCapital, int beginPeriodProfit, int transferFromOtherItem,
                              int transferToLegalSave, int transferToOtherSave, int plendPeriodDisCount, int sumCuttentYear,
int allocateProfit, int plendPeriodDisCountWithDes, int plpurePerShare, int profitPurePerShare, int cpital
                              )
        {
            this.PLPure = plpure;
            this.PLBeginPeriod = plbeginPeriod;
            this.DisCount = disCount;
            this.PLBeginPeriodDisCount = plbeginPeriodDisCount;
            this.SumLastYear = sumLastYear;
            this.ChangeCapital = changeCapital;
            this.BeginPeriodProfit = beginPeriodProfit;
            this.TransferFromOtherItem = transferFromOtherItem;
            this.TransferToLegalSave = transferToLegalSave;
            this.TransferToOtherSave = transferToOtherSave;
            this.PLEndPeriodDisCount = plendPeriodDisCount;
            this.SumCuttentYear = sumCuttentYear;
            this.AllocateProfit = allocateProfit;
            this.PLEndPeriodDisCountWithDes = plendPeriodDisCountWithDes;
            this.PLPurePerShare = plpurePerShare;
            this.ProfitPurePerShare = profitPurePerShare;
            this.Capital = Capital;
        }
        public int PLPure { get; private set; }
        public int PLBeginPeriod { get; private set; }
        public int DisCount { get; private set; }
        public int PLBeginPeriodDisCount { get; private set; }
        public int SumLastYear { get; private set; }
        public int ChangeCapital { get; private set; }
        public int BeginPeriodProfit { get; private set; }
        public int TransferFromOtherItem { get; private set; }
        public int AllocateProfit { get; private set; }
        public int TransferToLegalSave { get; private set; }
        public int TransferToOtherSave { get; private set; }
        public int PLEndPeriodDisCount { get; private set; }
        public int SumCuttentYear { get; private set; }
        public int PLEndPeriodDisCountWithDes { get; private set; }
        public int PLPurePerShare { get; private set; }
        public int ProfitPurePerShare { get; private set; }
        public int Capital { get; private set; }
    }
}