﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DomainModel.Entities.PartyAgreegate
{
    public class PartyModel
    {
        protected PartyModel()
        {

        }
        public PartyModel(string fullName)
        {
            this.FullName = fullName;
        }
        public int Id { get;private set; }
        public string FullName { get;private set; }
    }
}
