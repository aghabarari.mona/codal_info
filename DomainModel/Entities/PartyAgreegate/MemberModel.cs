﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DomainModel.Entities.PartyAgreegate
{
    public class MemberModel
    {
        public MemberModel()
        {

        }
        public MemberModel(string membershipType, string station, Boolean isBound)
        {
            this.MembershipType = membershipType;
            this.Station = station;
            this.IsBound = isBound;
        }
        public PartyModel Party { get; private set; }
        public string MembershipType { get; private set; }
        public string Station { get; private set; }
        public Boolean IsBound { get; private set; }

        public void SetParty(PartyModel party)
        {
            this.Party = party;
        }
        //public Address Address { get; private set; }

        //private readonly List<CreditCard> _creditCards;

        //public IReadOnlyCollection<CreditCard> CreditCards => _creditCards;
    }
}