﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DomainModel.Entities.PartyAgreegate
{
    public class PersonModel : PartyModel
    {
        private PersonModel()
        {

        }
        public PersonModel(string firstName, string lastName, string nationalCode, string diploma)
        {
            this.FirstName = firstName;
            this.LastName = lastName;
            this.NationalCode = nationalCode;
            this.Diploma = diploma;
        }
        public PartyModel Party { get; private set; }
        public string FirstName { get; private set; }
        public string LastName { get; private set; }
        public string NationalCode { get;private set; }
        public string Diploma { get; private set; }
    }
}
