﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DomainModel.Entities.PartyAgreegate
{
    public class CompanyModel : PartyModel
    {
        private CompanyModel()
        {

        }
        private CompanyModel(string companyType, long companyNo)
        {
            this.CompanyNo = companyNo;
            this.CompanyType = companyType;
        }
        public PartyModel Party { get; private set; }
        public string CompanyType { get; private set; }
        public long CompanyNo { get; private set; }
        public PersonModel Agent { get; private set; }
        private readonly List<MemberModel> _members;

        public IReadOnlyCollection<MemberModel> Members => _members;

        public void SetAgent(PersonModel agent)
        {
            this.Agent = agent;
        }

        public void AddMember(string membershipType, string station, Boolean isBound, PartyModel party)
        {
            var member = new MemberModel(membershipType, station, isBound);
            member.SetParty(party);
            _members.Add(member);
        }
    }
}
