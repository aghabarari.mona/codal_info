﻿using Application.Commands.User;
using Codal_Info.Security;
using DomainModel.Entities;
using Infrastructure.Utilities.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Http;

namespace Codal_Info.Controllers
{
    public class UserController : BaseApiController
    {
        private readonly ICreateUserHandler createUserHandler;

        public UserController(ICreateUserHandler createUserHandler)
        {
            this.createUserHandler = createUserHandler;
        }

        [Route("ping")]
        [HttpGet]
        [ApiAuth(Type = ApiAuthorizationType.ClientAndUser)]
        public IResult Ping()
        {
            return new Result();
        }

        [HttpGet]
        [Route("captcha")]
        public ApiResult GetCaptcha()
        {
            var tempToken = Guid.NewGuid().ToString();
            var res = "data:image/jpeg;base64," + GetCaptchaImage(tempToken);
            return ApiResult.CreateSuccessfully(new { tempToken, captcha = res });
        }

        private string GetCaptchaImage(string tmpToken)
        {
            var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
            var random = new Random();
            string captchaText = "";
            for (var i = 0; i < 5; i++)
            {
                var rnum = random.Next(chars.Count());
                captchaText += chars.Substring(rnum, 1);
            }

            SessionManager.SetCaptcha(tmpToken, captchaText);
            var path = @"~\files\captcha";
            var byts = new byte[90];// Captcha.GetCaptchaImage(captchaText, path, 45);
            var image = Convert.ToBase64String(byts);
            return image;
        }

        [HttpPost]
        [Route("login")]
        public ApiResult Login(UserModel model)
        {
            var token = Guid.NewGuid().ToString();
            SessionManager.SetSession(token, new UserModel(model.UserName,model.PassWord,model.FirstName,model.LastName,model.MobileNo));
            if (!ModelState.IsValid) return ApiResult.CreateWithModelStateError();

            return ApiResult.CreateSuccessfully(new { token });
        }



        [HttpPost]
        [Route("logout")]
        [ApiAuth(Type = ApiAuthorizationType.ClientAndMayUser)]
        public ApiResult Logout()
        {

            SessionManager.CloseSession(Token, ClientKey);
            return ApiResult.CreateSuccessfully();
        }

        [HttpPost]
        [Route("register")]
        public ApiResult Register(CreateUser model)
        {
            createUserHandler.Handler(model);
            return ApiResult.CreateSuccessfully();
        }
        public Boolean CheckPassword(string password)
        {
            var regex = @"^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$";
            var match = Regex.Match(password, regex, RegexOptions.IgnoreCase);

            if (!match.Success)
            {
                return false;
            }
            else
                return true;
        }

    }
}