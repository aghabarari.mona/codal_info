﻿using System;
using System.Drawing;
using System.IO;
using System.Web;

namespace Codal_Info.Controllers
{
    public class Captcha
    {

        static public byte[] GetCaptchaImage(string imageText, string path, int fontSize)
        {
            return ReadBytes(HttpContext.Current.Server.MapPath(GenerateCaptchaImage(imageText, path, fontSize)));
        }

        static private byte[] ReadBytes(string fileName)
        {
            byte[] b = new byte[0];

            if (File.Exists(fileName))
            {
                try
                {
                    FileStream s = File.OpenRead(fileName);
                    try
                    {
                        byte[] bytes = new byte[s.Length];
                        s.Read(bytes, (int)0, (int)s.Length);
                        b = bytes;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    finally
                    {
                        s.Close();
                    }
                }
                catch (IOException e)
                {
                    throw new IOException(e.Message);
                }
            }

            return b;
        }

        static private String GenerateCaptchaImage(string imageText, string path, int fontSize)
        {
            Font imgFont;
            int iIterate;
            Bitmap raster;

            Graphics graphicsObject;
            System.Drawing.Image imageObject = System.Drawing.Image.FromFile(HttpContext.Current.Server.MapPath(path + @"\imageBack.jpg"));

            // Create the Raster Image Object
            raster = new Bitmap(imageObject);

            //Create the Graphics Object
            graphicsObject = Graphics.FromImage(raster);

            //Instantiate object of brush with black color
            SolidBrush imgBrush = new SolidBrush(Color.Gray);

            //Add the characters to the image
            for (iIterate = 0; iIterate <= imageText.Length - 1; iIterate++)
            {
                imgFont = new Font("Arial", fontSize, FontStyle.Bold);
                String str = imageText.Substring(iIterate, 1);
                graphicsObject.DrawString(str, imgFont, imgBrush, iIterate * (fontSize - 5) + 45, 10);
                graphicsObject.Flush();
            }

            // Generate a uniqiue file name to save image as
            String fileName = "Captcha.gif";
            var filePath = path + @"\" + fileName;
            if (File.Exists(HttpContext.Current.Server.MapPath(filePath)))
                File.Delete(HttpContext.Current.Server.MapPath(filePath));
            raster.Save(HttpContext.Current.Server.MapPath(filePath), System.Drawing.Imaging.ImageFormat.Gif);
            raster.Dispose();
            graphicsObject = null;

            return filePath;

        }
    }
}
