﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.Commands.GeneralAssembly;
using Application.Queries.GeneralAssembly.GeneralAssemblyById;
using Application.Queries.GeneralAssembly.GeneralAssemblylist;
using Microsoft.AspNetCore.Mvc;

namespace Codal_Info.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        private readonly ICreateGeneralAssemblyHandler createGenealAssembly;
        private readonly IGetByIdGeneralAssemblyHandler getByIdGeneralAssemblylistHandler;
        private readonly IGetGeneralAssemblyHandler getGeneralAssemblyHandler;

        public ValuesController(ICreateGeneralAssemblyHandler createGeneralAssemblyHandler,
            IGetByIdGeneralAssemblyHandler getByIdGeneralAssemblylistHandler,
            IGetGeneralAssemblyHandler getGeneralAssemblyHandler)
        {
            this.createGenealAssembly = createGeneralAssemblyHandler;
            this.getByIdGeneralAssemblylistHandler = getByIdGeneralAssemblylistHandler;
            this.getGeneralAssemblyHandler = getGeneralAssemblyHandler;
        }
        [HttpGet]
        public Task<IList<GetGeneralAssemblylistResult>> Get()
        {
            return getGeneralAssemblyHandler.Handler(new GetGeneralAssemblylist());
        }

        [HttpGet("{id}")]
        public Task<GetByIdGeneralAssemblylistResult> Get(int id)
        {
            return getByIdGeneralAssemblylistHandler.Handler(new GetByIdGeneralAssemblylist()
            {
                Id = id
            });
        }

        [HttpPost]
        public void Post(CreateGeneralAssembly model)
        {
            createGenealAssembly.Handler(model);
        }

    }
}
