﻿using DomainModel.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore.Storage;
using System;

namespace Codal_Info.Security
{
    public class SessionManager
    {
        static string RedisConnection = "abortConnect=false";
        // static IDatabase rdb = ConnectionMultiplexer.Connect(SdpConfiguration.GetSetting("RedisConnection")).GetDatabase();
        //static IDatabase rdb = ConnectionMultiplexer.Connect(RedisConnection).GetDatabase();
        static ISession rdb =null;
        private static string _sessionKeyPrefix = "ussd_crm:session:token:";
        private static string _captchaKeyPrefix = "ussd_crm:login:captcha:tempToken:";
        private static string _ipKeyPrefix = "ussd_crm:login:session:ip:";


        public static void SetSession(string token, UserModel user)
        {
           // rdb.Set(_sessionKeyPrefix + token, user, new TimeSpan(0, 10, 0)); // one hour //TODO: hardCode
        }

        public static UserModel GetSession(string token)
        {
            // return rdb.GetObject<UserModel>(_sessionKeyPrefix + token);
            return null;
        }

        public static bool IsSessionExist(string token)
        {
            // return rdb.KeyExists(_sessionKeyPrefix + token);
            return false;

        }

        public static void RemoveSession(string token)
        {
            //rdb.KeyDelete(_sessionKeyPrefix + token);

        }

        public static void CloseSession(string token, string clientKey)
        {
            RemoveSession(token);
        }

        public static void RenewSessionExpiry(string token, string ip)
        {
            //rdb.KeyExpire(_sessionKeyPrefix + token, new TimeSpan(0, 10, 0));
            //rdb.KeyExpire(_ipKeyPrefix + ip, new TimeSpan(0, 10, 0));
        }

        public static void SetCaptcha(string tmpToken, string captchaText)
        {
            //rdb.StringSet(_captchaKeyPrefix + tmpToken, captchaText, new TimeSpan(0, 10, 0));
        }
        public static string GetCaptcha(string tmpToken)
        {
            // return rdb.StringGet(_captchaKeyPrefix + tmpToken);
            return null;
        }

        public static void SetIp(string ip, string username)
        {
            //rdb.StringSet(_ipKeyPrefix + ip, username, new TimeSpan(0, 10, 0));
        }

     

        public static void RemoveIp(string ip)
        {
            //rdb.KeyDelete(_ipKeyPrefix + ip);
        }
    }
}
