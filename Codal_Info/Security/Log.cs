﻿using System;
using System.Threading.Tasks;
using NLog;


namespace Codal_Info.Security
{
    public class Log
    {
        private static readonly NLog.Logger logger = LogManager.GetCurrentClassLogger();

        public static void Trace(string message, long? userId, string clientKey, string caller, Exception exception = null)
        {
            SaveLog(LogLevel.Trace, message, userId, clientKey, caller, exception);
        }
        public static void Debug(string message, long? userId, string clientKey, string caller, Exception exception = null)
        {
            SaveLog(LogLevel.Debug, message, userId, clientKey, caller, exception);
        }
        public static void Info(string message, long? userId, string clientKey, string caller, Exception exception = null)
        {
            SaveLog(LogLevel.Info, message, userId, clientKey, caller, exception);
        }
        public static void Warn(string message, long? userId, string clientKey, string caller, Exception exception = null)
        {
            SaveLog(LogLevel.Warn, message, userId, clientKey, caller, exception);
        }
        public static void Fatal(string message, long? userId, string clientKey, string caller, Exception exception = null)
        {
            SaveLog(LogLevel.Fatal, message, userId, clientKey, caller, exception);
        }
        public static void Error(string message, long? userId, string clientKey, string caller, Exception exception = null)
        {
            SaveLog(LogLevel.Error, message, userId, clientKey, caller, exception);
        }

        private static void SaveLog(LogLevel level, string message, long? userId, string clientKey, string caller, Exception exception)
        {
            new Task(() =>
            {
                LogEventInfo logEvent = new LogEventInfo(level, logger.Name, message);
                logEvent.Properties["UserId"] = userId;
                logEvent.Properties["ClientKey"] = clientKey;
                logEvent.Properties["Caller"] = exception?.StackTrace;
                logEvent.Exception = exception;
                logger.Log(logEvent);
            }).Start();
        }
    }
}
