﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Codal_Info.Security
{
    public enum ApiAuthorizationType
    {
        None = 0,
        JustClient = 1,
        ClientAndUser = 2,
        ClientAndMayUser = 3
    }
}