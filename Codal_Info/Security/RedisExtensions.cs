﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Microsoft.EntityFrameworkCore.Storage;
using Newtonsoft.Json;

namespace Codal_Info.Security
{
    public static class RedisExtensions
    {
        public static bool SetObject<T>(this IDatabase db, string key, T value, TimeSpan? expiry = null) where T : class
        {
            var serializedObject = JsonConvert.SerializeObject(value);
            return db.SetObject(key, serializedObject, expiry);
        }
        public static bool SetObjectWithBinarySerialization<T>(this IDatabase db, string key, T value, TimeSpan? expiry = null) where T : class
        {
            var bytes = ObjectToByteArray(value);
            var strValue = Convert.ToBase64String(bytes);
            return db.SetObject(key, strValue, expiry);
        }

        public static T GetObject<T>(this IDatabase db, string key) where T : class
        {
            var str = db.GetObject<string>(key);
            if (string.IsNullOrEmpty(str)) return null;
            return JsonConvert.DeserializeObject<T>(str, new JsonSerializerSettings() { });
        }
        public static T GetObjectWithBinaryDeserialization<T>(this IDatabase db, string key) where T : class
        {
            //var str = db.StringGet(key);
            //if (str.IsNullOrEmpty) return null;
            var str = db.GetObject<string>(key);
            if (string.IsNullOrEmpty(str)) return null;
            var bytes = Convert.FromBase64String(str);
            object obj = ByteArrayToObject(bytes);
            return (T)obj;
        }

        static byte[] ObjectToByteArray(object obj)
        {
            if (obj == null)
                return null;
            BinaryFormatter bf = new BinaryFormatter();
            using (MemoryStream ms = new MemoryStream())
            {
                bf.Serialize(ms, obj);
                return ms.ToArray();
            }
        }
        static Object ByteArrayToObject(byte[] arrBytes)
        {
            MemoryStream memStream = new MemoryStream();
            BinaryFormatter binForm = new BinaryFormatter();
            memStream.Write(arrBytes, 0, arrBytes.Length);
            memStream.Seek(0, SeekOrigin.Begin);
            Object obj = (Object)binForm.Deserialize(memStream);
            return obj;
        }
    }
}
