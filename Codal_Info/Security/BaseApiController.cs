﻿using Codal_Info.Security;
using System.Web.Http;

namespace Codal_Info
{
    public class BaseApiController : ApiController
    {
        protected long UserId
        {
            get
            {
                ApiAuthIdentity identity = (User as ApiAuthIdentity);

                if (identity != null) return identity.UserId;
                return 0;
            }
        }

        protected string Token
        {
            get
            {
                ApiAuthIdentity identity = (User.Identity as ApiAuthIdentity);

                if (identity != null) return identity.Token;
                return "";
            }
        }


        protected string ClientKey
        {
            get
            {
                ApiAuthIdentity identity = (User as ApiAuthIdentity);

                if (identity != null) return identity.ClientKey;
                return "";
            }
        }

    }
}
