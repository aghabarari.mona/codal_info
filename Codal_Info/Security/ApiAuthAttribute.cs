﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Principal;
using System.Threading;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace Codal_Info.Security
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false)]
    public class ApiAuthAttribute : AuthorizationFilterAttribute
    {
        public ApiAuthorizationType Type { get; set; }

        public override void OnAuthorization(HttpActionContext actionContext)
        {
            try
            {
                string token = null;
                var authRequest = actionContext.Request.Headers.Authorization;
                string[] ClientKeys = GetClientKeys();

                if (authRequest != null && !string.IsNullOrEmpty(authRequest.Scheme) && authRequest.Scheme == "Bearer")
                    token = authRequest.Parameter;
                var key = "";
                if (Type != ApiAuthorizationType.None)
                {
                    IEnumerable<string> ieKey = new List<string>();
                    if (actionContext.Request.Headers.TryGetValues("ClientKey", out ieKey))
                    {
                        key = ieKey.First();
                    }
                    if (Type == ApiAuthorizationType.JustClient)
                    {
                        if (ClientKeys.Contains(key))
                        {
                            var principal2 = new GenericPrincipal(new ApiAuthIdentity("", 0, key), new string[] { });
                            Thread.CurrentPrincipal = principal2;
                            actionContext.RequestContext.Principal = principal2;
                            base.OnAuthorization(actionContext);
                            return;
                        }
                        else
                        {
                            ChallengeAuthRequest(actionContext, key, token);
                            return;
                        }
                    }
                    if (Type == ApiAuthorizationType.ClientAndUser)
                    {
                        if (!ClientKeys.Contains(key))
                        {
                            ChallengeAuthRequest(actionContext, key, token);
                            return;
                        }
                    }
                    else if (Type == ApiAuthorizationType.ClientAndMayUser)
                    {
                        if (!ClientKeys.Contains(key))
                        {
                            ChallengeAuthRequest(actionContext, key, token);
                            return;
                        }
                    }
                }

                if (string.IsNullOrEmpty(token) || !SessionManager.IsSessionExist(token))
                {
                    if (Type != ApiAuthorizationType.ClientAndMayUser)
                    {
                        ChallengeAuthRequest(actionContext, key, token);
                        return;
                    }
                    else
                    {
                        var principal2 = new GenericPrincipal(new ApiAuthIdentity("23123123", 0, key), new string[] { });
                        Thread.CurrentPrincipal = principal2;
                        actionContext.RequestContext.Principal = principal2;
                        base.OnAuthorization(actionContext);
                        return;
                    }
                }

                string username = "";
                IEnumerable<string> ieUsername = new List<string>();
                if (actionContext.Request.Headers.TryGetValues("Username", out ieUsername))
                {
                    username = ieUsername.First();
                }
                string clientIp = "";
                var user = SessionManager.GetSession(token);
                if (user.UserName != username)
                {
                    ChallengeAuthRequest(actionContext, key, token);
                    return;
                }
                SessionManager.RenewSessionExpiry(token, clientIp);

                var roles = user.Roles.Select(s => s.ToString()).ToArray();
                var principal = new GenericPrincipal(new ApiAuthIdentity(token, user.UserID, key), roles);
                Thread.CurrentPrincipal = principal;
                actionContext.RequestContext.Principal = principal;

                base.OnAuthorization(actionContext);
            }
            catch (Exception ex)
            {
                Log.Error("error in auth", null, null, "auth", ex);
            }
        }

        private string[] GetClientKeys()
        {
            return new string[] { "1478963" };
        }

        private static void ChallengeAuthRequest(HttpActionContext actionContext, string clientKey, string token)
        {
            Log.Error($"Token:{token} ___ RequestUri:{actionContext.Request.RequestUri}", 0, clientKey, "401 Authorize");
            var dnsHost = actionContext.Request.RequestUri.DnsSafeHost;
            actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized);
            actionContext.Response.Headers.Add("WWW-Authenticate", string.Format("Bearer realm=\"{0}\"", dnsHost));
        }

       
    }


    public class ApiAuthIdentity : GenericIdentity
    {
        public string Token { get; set; }
        public long UserId { get; set; }
        public string ClientKey { get; set; }

        public ApiAuthIdentity(string token, long userId, string clientKey) : base(token)
        {
            Token = token;
            UserId = userId;
            ClientKey = clientKey;
        }
    }
}