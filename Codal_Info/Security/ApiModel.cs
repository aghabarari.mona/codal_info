﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;

namespace Codal_Info.Security
{
        public class DataError
        {
            public ErrorType ErrorType { get; set; }

            public int ErrorTypeCode => (int)ErrorType;

            public string Error
            {
                get
                {
                    var title = ErrorType.ToString();
                    return title;
                }
            }
            public string Message { get; set; }

            public DataError(ErrorType errorType, string message)
            {
                ErrorType = errorType;
                Message = message;
            }
        }

        public class ApiResult : HttpResponseMessage
        {
            public ApiResultType ResultType { get; set; }

            public bool result
            {
                get
                {
                    var title = false;
                    switch (ResultType)
                    {
                        case ApiResultType.Success:
                            title = true;
                            break;
                        case ApiResultType.Fail:
                            title = false;
                            break;
                        case ApiResultType.None:
                            title = false;
                            break;
                    }
                    return title;
                }
            }
            public object data { get; set; }
            public DataError error { get; set; }

            public ApiResult(ApiResultType resultType, object data = null, DataError error = null)
            {
                ResultType = resultType;
                this.data = data;
                this.error = error;

                if (error != null && error.ErrorType == ErrorType.AuthorizationError)
                {
                    StatusCode = HttpStatusCode.Unauthorized;
                }
                else if (error != null && error.ErrorType == ErrorType.Exception)
                {
                    StatusCode = HttpStatusCode.InternalServerError;
                }
                else if (error != null && error.ErrorType == ErrorType.ModelStateIsInvalid)
                {
                    StatusCode = HttpStatusCode.BadRequest;
                }
                else if (error != null && error.ErrorType == ErrorType.forbidden)
                {
                    StatusCode = HttpStatusCode.Forbidden;
                }
                if (data != null) Content = new ObjectContent(data.GetType(), data, new JsonMediaTypeFormatter());
                if (error != null) Content = new ObjectContent(error.Message.GetType(), error.Message, new JsonMediaTypeFormatter());
            }
            public ApiResult(ApiResultType resultType, object data)
            {
                ResultType = resultType;
                this.data = data;
                this.error = error;

                if (resultType == ApiResultType.Fail)
                {
                    StatusCode = HttpStatusCode.InternalServerError;
                }
                else if (resultType == ApiResultType.Success)
                {
                    StatusCode = HttpStatusCode.OK;
                }
                if (data != null) Content = new ObjectContent(data.GetType(), data, new JsonMediaTypeFormatter());
            }

            public static ApiResult CreateWithError(ErrorType errorType, string message)
            {
                return new ApiResult(ApiResultType.Fail, null, new DataError(errorType, message));
            }
            public static ApiResult CreateWithError(string message)
            {
                return new ApiResult(ApiResultType.Fail, null, new DataError(ErrorType.None, message));
            }
            public static ApiResult CreateWithError(object data)
            {
                return new ApiResult(ApiResultType.Fail, data);
            }
           
            public static ApiResult CreateWithException(string message)
            {
                return new ApiResult(ApiResultType.Fail, null, new DataError(ErrorType.Exception, message));
            }

            public static ApiResult CreateWithModelStateError()
            {
                return new ApiResult(ApiResultType.Fail, null, new DataError(ErrorType.ModelStateIsInvalid, "اطلاعات وارد شده کامل یا صحیح نیست."));
            }

            public static ApiResult CreateSuccessfully(object data)
            {
                return new ApiResult(ApiResultType.Success, data);
            }

            public static ApiResult CreateSuccessfully()
            {
                return new ApiResult(ApiResultType.Success, null);
            }
            public static ApiResult CreateUnauthorized(string message)
            {
                return new ApiResult(ApiResultType.Fail, null, new DataError(ErrorType.AuthorizationError, message));
            }
            public static ApiResult Createforbidden(string message)
            {
                return new ApiResult(ApiResultType.Fail, null, new DataError(ErrorType.forbidden, message));
            }

            private static string GetExceptionMessage(Exception ex)
            {
                return ex.Message +
                       (ex.InnerException != null
                           ? (" __ InnerException: " + ex.InnerException.Message) +
                             (ex.InnerException.InnerException != null
                                 ? (" __ InnerException2: " + ex.InnerException.InnerException.Message) +
                                   (ex.InnerException.InnerException.InnerException != null
                                       ? (" __ InnerException3: " + ex.InnerException.InnerException.InnerException.Message)
                                         + (ex.InnerException.InnerException.InnerException.InnerException != null
                                             ? (" __ InnerException4: " +
                                                ex.InnerException.InnerException.InnerException.InnerException.Message)
                                               +
                                               (ex.InnerException.InnerException.InnerException.InnerException
                                                   .InnerException !=
                                                null
                                                   ? (" __ InnerException5: " +
                                                      ex.InnerException.InnerException.InnerException.InnerException
                                                          .InnerException.Message)
                                                   : "")
                                             : "")
                                       : "")
                                 : "")
                           : "");
            }
        }



        public enum ErrorType
        {
            AuthorizationError = 1,
            ModelStateIsInvalid = 2,
            None = 3,
            Exception = 4,
            LoginFaild = 5,
            forbidden = 6
        };

        public enum ApiResultType
        {
            Success = 1,
            Fail = 2,
            None = 3
        }
    
}
