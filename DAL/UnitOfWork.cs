﻿using System.Threading.Tasks;
using  DAL;
using  DAL.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage;

namespace  Dal
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly IDbFactory dbFactory;
        private CodalInfoDbContext dbContext;

        public UnitOfWork(IDbFactory dbFactory)
        {
            this.dbFactory = dbFactory;
        }

       

        public async Task CommitAsync(IDbContextTransaction dbContextTransaction)
        {
            await dbFactory.CommitTransactionAsync(dbContextTransaction);

        }

        public void RejectChanges()
        {
            dbFactory.RollbackTransaction();

        }

        public Task<IDbContextTransaction> BeginTransaction()
        {
            return dbFactory.BeginTransactionAsync();

        }
    }
}
