﻿using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace  DAL.Infrastructure
{
    public interface IDbFactory : IDisposable
    {
        void RollbackTransaction();
        Task<IDbContextTransaction> BeginTransactionAsync();
        Task CommitTransactionAsync(IDbContextTransaction transaction);
        Task<bool> SaveEntitiesAsync();
    }
}
