﻿

using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Storage;

namespace  DAL.Infrastructure
{
    public class DbFactory : Disposable, IDbFactory
    {
        CodalInfoDbContext dbContext;

        public Task<IDbContextTransaction> BeginTransactionAsync()
        {
            throw new System.NotImplementedException();
        }

        public Task CommitTransactionAsync(IDbContextTransaction transaction)
        {
            throw new System.NotImplementedException();
        }

        public CodalInfoDbContext Init()
        {
            return dbContext ?? (dbContext = new CodalInfoDbContext(null));
        }

        public void RollbackTransaction()
        {
            throw new System.NotImplementedException();
        }

        public Task<bool> SaveEntitiesAsync()
        {
            throw new System.NotImplementedException();
        }

        protected override void DisposeCore()
        {
            if (dbContext != null)
                dbContext.Dispose();
        }
    }
}
