﻿using Microsoft.EntityFrameworkCore.Storage;
using System.Threading.Tasks;

namespace  Dal
{
    public interface IUnitOfWork
    {
        Task CommitAsync(IDbContextTransaction dbContextTransaction);
        void RejectChanges();
        Task<IDbContextTransaction> BeginTransaction();
    }
}
