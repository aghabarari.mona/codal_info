﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace  Repository
{
    public interface IRepository<T> where T : class
    {

        T CreateNew();
        // Marks an entity as new
        void Add(T entity);
        // Marks an entity as modified
        void Update(T entity);
        // Marks an entity to be removed
        void Delete(T entity);
        void Delete(Expression<Func<T, bool>> where);
        // Get an entity by int id
        T GetById(object id);
        // Get an entity using delegate
        T Get(Expression<Func<T, bool>> where);
        Task<T> GetAsync(Expression<Func<T, bool>> where);
        // Gets all entities of type T
        Task<IEnumerable<T>> GetAllAsync();
        IEnumerable<T> GetAll();
        IQueryable<T> GetAllIQueryable();
        Task<int> GetCountAsync(Expression<Func<T, bool>> where);
        int GetCount(Expression<Func<T, bool>> where);
        Task<bool> IsExistsAsync(Expression<Func<T, bool>> where);
        bool IsExists(Expression<Func<T, bool>> where);
        // Gets entities using delegate
        Task<IEnumerable<T>> GetManyAsync(Expression<Func<T, bool>> where);
        IEnumerable<T> GetMany(Expression<Func<T, bool>> where);
        Task<IEnumerable<T>> SearchByFilterAsync(Utility.DynamicSearch.SearchFilterMaker sf);
        IEnumerable<T> SearchByFilter(Utility.DynamicSearch.SearchFilterMaker sf);
        IEnumerable<T> SearchByFilterByPaging(Utility.DynamicSearch.SearchFilterMaker sf);
        Task<int> GetCountAsync(Utility.DynamicSearch.SearchFilterMaker sf);
        int GetCount(Utility.DynamicSearch.SearchFilterMaker sf);
        Task<bool> IsExistsAsync(Utility.DynamicSearch.SearchFilterMaker sf);
        bool IsExists(Utility.DynamicSearch.SearchFilterMaker sf);
        int GetCount();
        Task<IEnumerable<TResult>> GetManyAsync<TResult>(Expression<Func<T, TResult>> selector,
                                        Expression<Func<T, bool>> where = null,
                                        string include = "",
                                        int taked = 0,
                                        int skiped = 0,
                                        bool TrackingChanges = true);
        IEnumerable<TResult> GetMany<TResult>(Expression<Func<T, TResult>> selector,
                                        Expression<Func<T, bool>> where = null,
                                        string include = "",
                                        int taked = 0,
                                        int skiped = 0,
                                        bool TrackingChanges = true);
        IEnumerable<T> Get(Expression<Func<T, bool>> filter = null,
            Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, string includes = "");
        #region Extended Methods
        //int BatchDelete(Expression<Func<T, bool>> where);
        //Task<int> BatchDeleteAsync(Expression<Func<T, bool>> where);
        //int BatchUpdate(Expression<Func<T, bool>> where, Expression<Func<T, T>> updateSet);
        //Task<int> BatchUpdateAsync(Expression<Func<T, bool>> where, Expression<Func<T, T>> updateSet);
        #endregion
    }
}
