﻿using DomainModel.Entities;
using DomainModel.Entities.PartyAgreegate;
using System;
using System.Collections.Generic;
using System.Text;

namespace Repository.IRepositoris
{
    public interface IMemberRepository
    {
        IEnumerable<MemberModel> GetAllmembers();
        MemberModel GetById(int id);
    }
}
