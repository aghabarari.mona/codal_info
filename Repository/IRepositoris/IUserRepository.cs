﻿using System.Collections.Generic;
using System.Threading.Tasks;
using  DomainModel.Entities;

namespace  Repository
{
    public interface IUserRepository
    {
        IEnumerable<UserModel> GetAllUsers();
        UserModel GetByUserName(string userName);
        UserModel GetByUserId(int userId);
        Task Add(UserModel entity);
    }
}
