﻿using DomainModel.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Repository.IRepositoris
{
    public interface IGeneralAssemblyRepository
    {
        IEnumerable<InformationModel> GetAll();
        InformationModel GetById(int id);
        Task Add(InformationModel entity);
        void Update(InformationModel entity);
    }
}
