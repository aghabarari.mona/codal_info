﻿using DomainModel.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Repository.IRepositoris
{
    public interface IDecisionsModelRepository
    {
        IEnumerable<DecisionsModel> GetAll();
        DecisionsModel GetById(int id);
        Task Add(DecisionsModel entity);
        void Update(DecisionsModel entity);
    }
}
