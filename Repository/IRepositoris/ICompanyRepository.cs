﻿using DomainModel.Entities.PartyAgreegate;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Repository.IRepositoris
{
    public interface ICompanyRepository
    {
        IEnumerable<CompanyModel> GetAll();
        CompanyModel GetById(int id);
        Task Add(CompanyModel entity);
        void Update(CompanyModel entity);
    }
}
