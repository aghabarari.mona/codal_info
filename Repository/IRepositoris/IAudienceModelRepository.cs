﻿using DomainModel.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Repository.IRepositoris
{
    public interface IAudienceModelRepository
    {
        IEnumerable<AudienceModel> GetAll();
        AudienceModel GetById(int id);
        Task Add(AudienceModel entity);
        void Update(AudienceModel entity);
    }
}
