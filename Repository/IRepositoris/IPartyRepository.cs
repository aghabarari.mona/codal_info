﻿using DomainModel.Entities.PartyAgreegate;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Repository.IRepositoris
{
    public interface IPartyRepository
    {
        IEnumerable<PartyModel> GetAllParties();
        PartyModel GetById(int id);
        Task Add(PartyModel entity);
        void Update(PartyModel entity);
    }
}
