﻿using  DAL.Infrastructure;
using System;


namespace  Repository
{
    public static class RepositoyFactory
    {
        public static IUserRepository GetUserRepository(IDbFactory dbFactory)
        {
            return new UserRepository(dbFactory);
        }
    }
}
