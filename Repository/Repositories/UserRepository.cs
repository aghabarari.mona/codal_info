﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DAL.Infrastructure;
using DomainModel.Entities;

namespace  Repository
{
    public class UserRepository :  IUserRepository
    {
        

        #region Base Methods
        public IEnumerable<UserModel> GetAllUsers()
        {
            return null;
        }

        public UserModel GetByUserName(string userName)
        {
            return null;
        }

        public UserModel GetByUserId(int userId)
        {
            return null;
        }
        #endregion


        #region Other Methods
        public void Add(UserModel entity)
        {
            
        }
        
        public void Update(UserModel entity)
        {
            
        }
       
        #endregion
        UserModel IUserRepository.GetByUserName(string userName)
        {
            return null;
        }
        UserModel IUserRepository.GetByUserId(int userId)
        {
            return null;
        }

        Task IUserRepository.Add(UserModel entity)
        {
            throw new NotImplementedException();
        }
    }
}
