﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Data.Entity;
using System.Linq.Expressions;
using System.Text;
using System.Transactions;
using System.Threading.Tasks;
using  DAL;
using  DAL.Infrastructure;

namespace  Repository
{
    public abstract class RepositoryBase<T>:IRepository<T> where T : class
    {
        #region Properties
        private TestDbContext dataContext;
        private readonly IDbSet<T> dbSet;

        protected IDbFactory DbFactory
        {
            get;
            private set;
        }

        protected TestDbContext DbContext
        {
            get { return dataContext ?? (dataContext = DbFactory.Init()); }
        }
        #endregion

        protected RepositoryBase(IDbFactory dbFactory)
        {
            DbFactory = dbFactory;
            dbSet = DbContext.Set<T>();
        }
        #region Implementation
        public virtual T CreateNew()
        {
            return dbSet.Create();
        }
        #region Add and Update
        public virtual void Add(T entity)
        {
            dbSet.Add(entity);
        }

        public virtual void Update(T entity)
        {
            dbSet.Attach(entity);
            dataContext.Entry(entity).State = System.Data.Entity.EntityState.Modified;
        } 
        #endregion

        #region Delete
        public virtual void Delete(T entity)
        {
            if (dataContext.Entry(entity).State == System.Data.Entity.EntityState.Detached)
            {
                dbSet.Attach(entity);
            }
            dbSet.Remove(entity);
        }

        public virtual void Delete(Expression<Func<T, bool>> where)
        {
            IEnumerable<T> objects = dbSet.Where<T>(where).AsEnumerable();
            foreach (T obj in objects)
                dbSet.Remove(obj);
        } 
        #endregion

        #region Get
        public virtual T GetById(object id)
        {
            return dbSet.Find(id);
        }

        public virtual async Task<IEnumerable<T>> GetAllAsync()
        {
            return await dbSet.ToListAsync();
        }
        public virtual IEnumerable<T> GetAll()
        {
            return dbSet.ToList();
        }
        public virtual IQueryable<T> GetAllIQueryable()
        {
            return dbSet as IQueryable<T>;
        }


        public virtual async Task<IEnumerable<T>> GetManyAsync(Expression<Func<T, bool>> where)
        {
            return await dbSet.Where(where).ToListAsync();
        }
        public virtual IEnumerable<T> GetMany(Expression<Func<T, bool>> where)
        {
            return dbSet.Where(where).ToList();
        }
        public virtual async Task<T> GetAsync(Expression<Func<T, bool>> where)
        {
            return await dbSet.Where(where).FirstOrDefaultAsync<T>();
        }
        public virtual T Get(Expression<Func<T, bool>> where)
        {
            return dbSet.Where(where).FirstOrDefault<T>();
        }
        public virtual IEnumerable<T> Get(Expression<Func<T, bool>> filter = null,
            Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, string includes = "")
        {
            IQueryable<T> query = dbSet;
            if (filter != null)
                query = query.Where(filter);
            if (orderBy != null)
                query = orderBy(query);
            foreach (string include in includes.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(include);
            }
            return query.ToList();

        }
        #endregion

        #region Raw Query
        public virtual async Task<IEnumerable<T>> SearchByFilterAsync(Utility.DynamicSearch.SearchFilterMaker sf)
        {
            return await DbContext.Database.SqlQuery<T>(sf.Criteria).ToListAsync();
        }
        public virtual IEnumerable<T> SearchByFilter(Utility.DynamicSearch.SearchFilterMaker sf)
        {
            return DbContext.Database.SqlQuery<T>(sf.Criteria).ToList();
        }
        public virtual IEnumerable<T> SearchByFilterByPaging(Utility.DynamicSearch.SearchFilterMaker sf)
        {
            return DbContext.Database.SqlQuery<T>(sf.Criteria).Skip(sf.Skip).Take(sf.Take).ToList();
        } 
        #endregion

        #region Count
        public virtual async Task<int> GetCountAsync(Expression<Func<T, bool>> where)
        {
            return await dbSet.CountAsync(where);
        }
        public virtual int GetCount(Expression<Func<T, bool>> where)
        {
            return dbSet.Count(where);
        }
        public virtual async Task<int> GetCountAsync(Utility.DynamicSearch.SearchFilterMaker sf)
        {
            return await DbContext.Database.SqlQuery<T>(sf.Criteria).CountAsync();
        }
        public virtual int GetCount(Utility.DynamicSearch.SearchFilterMaker sf)
        {
            return DbContext.Database.SqlQuery<T>(sf.Criteria).Count();
        }
        public virtual async Task<int> GetCountAsync()
        {
            return await dbSet.CountAsync();
        }
        public virtual int GetCount()
        {
            return dbSet.Count();
        }
        #endregion

        #region Exist
        public virtual async Task<bool> IsExistsAsync(Expression<Func<T, bool>> where)
        {
            return await dbSet.AnyAsync(where);
        }
        public virtual bool IsExists(Expression<Func<T, bool>> where)
        {
            return dbSet.Any(where);
        }
        public virtual async Task<bool> IsExistsAsync(Utility.DynamicSearch.SearchFilterMaker sf)
        {
            return await DbContext.Database.SqlQuery<T>(sf.Criteria).AnyAsync();
        }
        public virtual bool IsExists(Utility.DynamicSearch.SearchFilterMaker sf)
        {
            return DbContext.Database.SqlQuery<T>(sf.Criteria).Any();
        } 
        #endregion

        #region Get Custom Fields By Filter
        public async Task<IEnumerable<TResult>> GetManyAsync<TResult>(Expression<Func<T, TResult>> selector,
                                        Expression<Func<T, bool>> where = null,
                                        string include = "",
                                        int taked = 0,
                                        int skiped = 0,
                                        bool TrackingChanges = true)
        {
            IQueryable<T> query = dbSet;
            if (where != null)
                query = query.Where(where);
            #region Include

            foreach (var includeProperty in include.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            #endregion
            if (skiped != 0)
                query = query.Skip(taked);
            if (taked != 0)
                query = query.Take(taked);
            if (TrackingChanges)
                return await query.Select(selector).ToListAsync();
            return await query.AsNoTracking().Select(selector).ToListAsync();
        }
        public IEnumerable<TResult> GetMany<TResult>(Expression<Func<T, TResult>> selector,
                                        Expression<Func<T, bool>> where = null,
                                        string include = "",
                                        int taked = 0,
                                        int skiped = 0,
                                        bool TrackingChanges = true)
        {
            IQueryable<T> query = dbSet;
            if (where != null)
                query = query.Where(where);
            #region Include

            foreach (var includeProperty in include.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            #endregion
            if (skiped != 0)
                query = query.Skip(taked);
            if (taked != 0)
                query = query.Take(taked);
            if (TrackingChanges)
                return query.Select(selector).ToList();
            return query.AsNoTracking().Select(selector).ToList();

        }
        #endregion

        #endregion

        #region Extended Methods
        //public virtual int BatchDelete(Expression<Func<T, bool>> where)
        //{
        //    return dbSet.Where(where).Delete();
        //}
        //public virtual Task<int> BatchDeleteAsync(Expression<Func<T, bool>> where)
        //{
        //    return dbSet.Where(where).DeleteAsync();
        //}
        //public virtual int BatchUpdate(Expression<Func<T, bool>> where, Expression<Func<T, T>> updateSet)
        //{
        //    return dbSet.Where(where).Update(updateSet);
        //}
        //public virtual Task<int> BatchUpdateAsync(Expression<Func<T, bool>> where, Expression<Func<T, T>> updateSet)
        //{
        //    return dbSet.Where(where).UpdateAsync(updateSet);
        //}
        #endregion

    }
}
