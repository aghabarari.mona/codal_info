﻿using Infrastructure.Utilities.Queries;
using Repository.IRepositoris;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Queries.GeneralAssembly.GeneralAssemblylist
{
   public class GetGeneralAssemblylistHandler : ListHandler<GetGeneralAssemblylist, GetGeneralAssemblylistResult>, IGetGeneralAssemblyHandler
    {
        private readonly IGeneralAssemblyRepository _generalAssemblyRepository;
        public GetGeneralAssemblylistHandler(IGeneralAssemblyRepository generalAssemblyRepository)
        {
            _generalAssemblyRepository = generalAssemblyRepository;
        }

        public override async Task<IList<GetGeneralAssemblylistResult>> Handler(GetGeneralAssemblylist message)
        {
            var results = new List<GetGeneralAssemblylistResult>();
            var model = _generalAssemblyRepository.GetAll().ToList();

            results = model.Select(x=> new GetGeneralAssemblylistResult {
                Address = x.Address,
            CompanyId = x.Company.Id,
            Date = x.Date,
            Day = x.Day,
            EndTime = x.EndTime,
            StartDate = x.StartDate,
            StartTime = x.StartTime
        }).ToList();
            
            return results;
        }
    }

    public interface IGetGeneralAssemblyHandler
    {
        Task<IList<GetGeneralAssemblylistResult>> Handler(GetGeneralAssemblylist message);
    }
}
