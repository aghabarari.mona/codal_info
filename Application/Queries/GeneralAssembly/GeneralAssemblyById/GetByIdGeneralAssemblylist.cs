﻿using DomainModel.Entities;
using Infrastructure.Utilities.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Queries.GeneralAssembly.GeneralAssemblyById
{
    public class GetByIdGeneralAssemblylist : Request<GetByIdGeneralAssemblylistResult>
    {
        public int Id { get; set; }

    }

    public class GetByIdGeneralAssemblylistResult:Result
    {
        public string EndTime { get; set; }
        public int CompanyId { get; set; }
        public string Symbol { get; set; }
        public string StartDate { get; set; }
        public string StartTime { get; set; }
        public string Day { get; set; }
        public string Date { get; set; }
        public string Address { get; set; }
        public string Order { get; set; }
        public List<int> Audiences { get; set; }
        public DecisionsModel Decision { get; set; }
    }
}
