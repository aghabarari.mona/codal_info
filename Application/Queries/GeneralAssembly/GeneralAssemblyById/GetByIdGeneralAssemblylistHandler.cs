﻿using Application.Commands.GeneralAssembly;
using Infrastructure.Utilities.Queries;
using Repository.IRepositoris;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Application.Queries.GeneralAssembly.GeneralAssemblyById
{
   public class GetByIdGeneralAssemblylistHandler : SingleHandler<GetByIdGeneralAssemblylist, GetByIdGeneralAssemblylistResult>, IGetByIdGeneralAssemblyHandler
    {
        private readonly IGeneralAssemblyRepository _generalAssemblyRepository;
        public GetByIdGeneralAssemblylistHandler(IGeneralAssemblyRepository generalAssemblyRepository)
        {
            _generalAssemblyRepository = generalAssemblyRepository;
        }

        public override async Task<GetByIdGeneralAssemblylistResult> Handler(GetByIdGeneralAssemblylist message)
        {
            var result = new GetByIdGeneralAssemblylistResult();
            var model = _generalAssemblyRepository.GetById(message.Id);

            result.Address = model.Address;
            result.CompanyId = model.Company.Id;
            result.Date = model.Date;
            result.Day = model.Day;
            result.EndTime = model.EndTime;
            result.StartDate = model.StartDate;
            result.StartTime = model.StartTime;
            return result;
        }
    }

    public interface IGetByIdGeneralAssemblyHandler
    {
        Task<GetByIdGeneralAssemblylistResult> Handler(GetByIdGeneralAssemblylist message);
    }
}
