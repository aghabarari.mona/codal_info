﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Application {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Validations {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Validations() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Application.Validations", typeof(Validations).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to نام نماینده حقوقی اعضا اجباری است.
        /// </summary>
        public static string AgentNameMemberRequired {
            get {
                return ResourceManager.GetString("AgentNameMemberRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to شماره ملی نماینده اعضا اجباری است.
        /// </summary>
        public static string AgentNationalCodeMemberRequired {
            get {
                return ResourceManager.GetString("AgentNationalCodeMemberRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to تکمیل آدرس اجباری است.
        /// </summary>
        public static string GeneralAssemblyAddressRequired {
            get {
                return ResourceManager.GetString("GeneralAssemblyAddressRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to نام شرکت اجباری است.
        /// </summary>
        public static string GeneralAssemblyCompanyNameRequired {
            get {
                return ResourceManager.GetString("GeneralAssemblyCompanyNameRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to تکمیل زمان اجباری است.
        /// </summary>
        public static string GeneralAssemblyDateRequired {
            get {
                return ResourceManager.GetString("GeneralAssemblyDateRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to تکمیل روز اجباری است.
        /// </summary>
        public static string GeneralAssemblyDayRequired {
            get {
                return ResourceManager.GetString("GeneralAssemblyDayRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to تاریخ انتهای دوره مالی اجباری است.
        /// </summary>
        public static string GeneralAssemblyEndTimeRequired {
            get {
                return ResourceManager.GetString("GeneralAssemblyEndTimeRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to اجباری است.
        /// </summary>
        public static string GeneralAssemblyOrderRequired {
            get {
                return ResourceManager.GetString("GeneralAssemblyOrderRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to تاریخ اجباری است.
        /// </summary>
        public static string GeneralAssemblyStartDateRequired {
            get {
                return ResourceManager.GetString("GeneralAssemblyStartDateRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to زمان اجباری است.
        /// </summary>
        public static string GeneralAssemblyStartTimeRequired {
            get {
                return ResourceManager.GetString("GeneralAssemblyStartTimeRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to نماد اجباری است.
        /// </summary>
        public static string GeneralAssemblySymbolRequired {
            get {
                return ResourceManager.GetString("GeneralAssemblySymbolRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to نام اعضا اجباری است.
        /// </summary>
        public static string MemberNameRequired {
            get {
                return ResourceManager.GetString("MemberNameRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to شماره ثبت اجباری است.
        /// </summary>
        public static string MemberNumberRequired {
            get {
                return ResourceManager.GetString("MemberNumberRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to نوع عضویت اجباری است.
        /// </summary>
        public static string MembershipTypeRequired {
            get {
                return ResourceManager.GetString("MembershipTypeRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to نام شرکت اجباری است.
        /// </summary>
        public static string PartyFullNameRequired {
            get {
                return ResourceManager.GetString("PartyFullNameRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to تکمیل سمت اعضا اجباری است.
        /// </summary>
        public static string StationMemberRequired {
            get {
                return ResourceManager.GetString("StationMemberRequired", resourceCulture);
            }
        }
    }
}
