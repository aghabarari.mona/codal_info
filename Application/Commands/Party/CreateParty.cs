﻿using Infrastructure.Utilities.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Commands.Party
{
    public class CreateParty : Request<Result>
    {
        public int Id { get; set; }
        public string FullName { get; set; }
    }
}
