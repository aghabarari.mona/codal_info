﻿using Dal;
using DomainModel.Entities.PartyAgreegate;
using Infrastructure.Utilities.Commands;
using Infrastructure.Utilities.Common;
using Repository.IRepositoris;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Application.Commands.Party
{
    class CreatePartyHandler : CommandHandler<CreateParty, Result>, ICreatePartyHandler
    {
        private readonly IPartyRepository _partyRepository;
        private readonly IUnitOfWork _unitOfWork;
        public CreatePartyHandler(IPartyRepository partyRepository)
        {
            _partyRepository = partyRepository;
        }

        public override async Task<Result> Handler(CreateParty message)
        {
            var partyModel = new PartyModel(message.FullName);
            var tran = await _unitOfWork.BeginTransaction();
            await _partyRepository.Add(partyModel);
            await _unitOfWork.CommitAsync(tran);

            return new Result();
        }
    }

    public interface ICreatePartyHandler
    {
        Task<Result> Handler(CreateParty message);
    }
}
