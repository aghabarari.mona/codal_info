﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Commands.Party
{
    class CreatePartyValiation : AbstractValidator<CreateParty>
    {
        public CreatePartyValiation()
        {
            RuleFor(x => x.FullName).NotNull().WithMessage(Validations.PartyFullNameRequired);
        }
    }
}
