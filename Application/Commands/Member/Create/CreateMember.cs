﻿using Infrastructure.Utilities.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Commands.Member.Create
{
    public class CreateMember: Request<Result>
    {
        public string MemberName { get; set; }
        public string MemberNumber { get; set; }
        public string CompanyType { get; set; }
        public string MembershipType { get; set; }
        public string AgentName { get; set; }
        public String AgentNationalCode { get; set; }
        public string Station { get; set; }
        public Boolean IsBound { get; set; }
        public string Diploma { get; set; }
    }
}
