﻿using DomainModel.Entities.PartyAgreegate;
using Infrastructure.Utilities.Commands;
using Infrastructure.Utilities.Common;
using Repository.IRepositoris;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Application.Commands.Member.Create
{
    public class CreateMemberHandler : CommandHandler<CreateMember, Result>, ICreateMemberHandler
    {
        private readonly IMemberRepository _MemberRepository;

        public CreateMemberHandler(IMemberRepository MemberRepository)
        {
            _MemberRepository = MemberRepository;
        }

        public override async Task<Result> Handler(CreateMember message)
        {
            //var Member = new MemberModel(message.FirstName, message.LastName, message.Email, message.IsActive);
            //await _MemberRepository.AddAsync(Member);
            //await _MemberRepository.SaveEntitiesAsync();

            return new Result();
        }
    }

    public interface ICreateMemberHandler
    {
        Task<Result> Handler(CreateMember message);
    }
}
