﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Commands.Member.Create
{
    class CreateMemberValiation : AbstractValidator<CreateMember>
    {
        public CreateMemberValiation()
        {
            RuleFor(x => x.MemberName).NotNull().WithMessage(Validations.MemberNameRequired);
            RuleFor(x => x.MemberNumber).NotNull().WithMessage(Validations.MemberNumberRequired);
            RuleFor(x => x.MembershipType).NotNull().WithMessage(Validations.MembershipTypeRequired);
            //RuleFor(x => x.AgentName).NotNull().WithMessage(Validations.AgentNameMemberRequired);
            //RuleFor(x => x.AgentNationalCode).NotNull().WithMessage(Validations.AgentNationalCodeMemberRequired);
            RuleFor(x => x.Station).NotNull().WithMessage(Validations.StationMemberRequired);
        }
    }
}
