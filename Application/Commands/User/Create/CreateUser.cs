﻿using Infrastructure.Utilities.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Commands.User
{
   public class CreateUser : Request<Result>
    {
        public int UserID { get; set; }
        public string UserName { get; set; }
        public string PassWord { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MobileNo { get; set; }

    }
}
