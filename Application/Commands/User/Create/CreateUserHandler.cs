﻿using Dal;
using DomainModel.Entities;
using Infrastructure.Utilities.Commands;
using Infrastructure.Utilities.Common;
using Repository;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Application.Commands.User
{
    class CreateUserHandler : CommandHandler<CreateUser, Result>, ICreateUserHandler
    {
        private readonly IUserRepository _userRepository;
        private readonly IUnitOfWork _unitOfWork;
        public CreateUserHandler(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public IUnitOfWork UnitOfWork => _unitOfWork;

        public override async Task<Result> Handler(CreateUser message)
        {
            var userModel = new UserModel(message.UserName,message.PassWord,message.FirstName,message.LastName,message.MobileNo);
            var tran = await UnitOfWork.BeginTransaction();
            await _userRepository.Add(userModel);
            await UnitOfWork.CommitAsync(tran);

            return new Result();
        }
    }

    public interface ICreateUserHandler
    {
        Task<Result> Handler(CreateUser message);
    }
}
