﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Commands.GeneralAssembly
{
    class CreateGeneralAssemblyValidation : AbstractValidator<CreateGeneralAssembly>
    {
        public CreateGeneralAssemblyValidation()
        {
            RuleFor(x => x.EndTime).NotNull().WithMessage(Validations.GeneralAssemblyEndTimeRequired);
            RuleFor(x => x.Symbol).NotNull().WithMessage(Validations.GeneralAssemblySymbolRequired);
            RuleFor(x => x.StartDate).NotNull().WithMessage(Validations.GeneralAssemblyStartDateRequired);
            RuleFor(x => x.StartTime).NotNull().WithMessage(Validations.GeneralAssemblyStartTimeRequired);
            RuleFor(x => x.Day).NotNull().WithMessage(Validations.GeneralAssemblyDayRequired);
            RuleFor(x => x.Date).NotNull().WithMessage(Validations.GeneralAssemblyDateRequired);
            RuleFor(x => x.Address).NotNull().WithMessage(Validations.GeneralAssemblyAddressRequired);
            RuleFor(x => x.Order).NotNull().WithMessage(Validations.GeneralAssemblyOrderRequired);
        }
    }
}
