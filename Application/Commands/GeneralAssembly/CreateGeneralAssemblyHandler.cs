﻿using Dal;
using DomainModel.Entities;
using Infrastructure.Utilities.Commands;
using Infrastructure.Utilities.Common;
using Repository.IRepositoris;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Application.Commands.GeneralAssembly
{
    public class CreateGeneralAssemblyHandler : CommandHandler<CreateGeneralAssembly, Result>, ICreateGeneralAssemblyHandler
    {
        private readonly IGeneralAssemblyRepository _Repository;
        private readonly ICompanyRepository _companyRepository;
        private readonly IDecisionsModelRepository decisionsModelRepository;
        private readonly IAudienceModelRepository audienceModelRepository;
        private readonly IUnitOfWork _unitOfWork;
        public CreateGeneralAssemblyHandler(IGeneralAssemblyRepository partyRepository,
            ICompanyRepository companyRepository,
            IDecisionsModelRepository decisionsModelRepository,
            IAudienceModelRepository audienceModelRepository)
        {
            _companyRepository = companyRepository;
            this.decisionsModelRepository = decisionsModelRepository;
            this.audienceModelRepository = audienceModelRepository;
            _Repository = partyRepository;
        }

        public override async Task<Result> Handler(CreateGeneralAssembly message)
        {
            var InformationModel = new InformationModel(message.EndTime,message.Symbol,message.StartDate,
                                                        message.Day,message.Date,message.Address,message.Order);
            var company = _companyRepository.GetById(message.CompanyId);
            InformationModel.SetCompany(company);

            var Audiences = new List<AudienceModel>();

            message.Audiences.ForEach(x => {
                var au = audienceModelRepository.GetById(x);
                InformationModel.AddAudience(au);

            });


            InformationModel.SetDecisionsModel(message.Decision);
            var tran = await _unitOfWork.BeginTransaction();
            await _Repository.Add(InformationModel);
            await _unitOfWork.CommitAsync(tran);

            return new Result();
        }
    }

    public interface ICreateGeneralAssemblyHandler
    {
        Task<Result> Handler(CreateGeneralAssembly message);
    }
}
